import 'dart:async';
import 'dart:typed_data';
import 'package:flutter/material.dart';
import 'package:flutter_blue/flutter_blue.dart';
import 'package:flutter_blue_example/Data.dart';

class DeviceScreen extends StatelessWidget {
  DeviceScreen({
    Key key,
    this.device,
  }) : super(key: key);

  final BluetoothDevice device;
  static const UUID_service = 'be940000-7333-be46-b7ae-689e71722bd5';
  static const UUID = 'be940001-7333-be46-b7ae-689e71722bd5';
  static const UUID3 = 'be940003-7333-be46-b7ae-689e71722bd5';
  static const dataSport = ([3, 2, 7, 0, 0, 38, -105]);
  static const dataSport2 = ([3, 9, 9, 0, 1, 0, 3, -127, -50]);
  static const List<int> dataSalud = ([3, 2, 7, 0, 2, 100, -73]);
  static const List<int> dataSalud2 = ([3, 9, 9, 0, 1, 3, 5, 20, -5]);
  static const List<int> stopMeasuring = ([3, 2, 7, 0, 0, 38, -105]);
  var steps = 0;
  var kcal = 0;
  dynamic km = 0;
  var systolic = 0;
  var diastolic = 0;
  var hr = 0;
  BluetoothCharacteristic tx;
  BluetoothCharacteristic txNotify;

  getCharacteristic() async {
    print('------------------------getCharacteristic');
    List<BluetoothService> services = await device.discoverServices();
    services.forEach((service) async {
      if (service.uuid.toString() == UUID_service.toString()) {
        // do something with service
        var characteristics = service.characteristics;
        print('----------Caracteristicas:');
        for (BluetoothCharacteristic c in characteristics) {
          if (c.uuid.toString() == UUID.toString()) {
            print('******----------------ENCONTRADO--------------******');
            tx = c;
          } else {
            print('No se ha encotrado la caracteristica');
          }

          if (c.uuid.toString() == UUID3.toString()) {
            print('******--------------ENCONTRADO---------------******');

            txNotify = c;
          } else {
            print('No se ha encotrado la caracteristica');
          }
          print('----------------------------:');
        }
      } else {
        print('No se ha encotrado el servicio');
      }
    });
  }

  void getSport() async {
    await getCharacteristic();
    await tx.write(dataSport);
    await tx.write(dataSport2);

    txNotify.setNotifyValue(true);

    try {
      txNotify.value.listen((value) {
        // do something with new value
        Uint8List input = Uint8List.fromList(value);
        ByteData bd = input.buffer.asByteData();

        steps = bd.getUint16(4, Endian.little);
        km = bd.getUint16(6, Endian.little) / 1000;
        kcal = bd.getUint16(8, Endian.little);

        print(steps);
        print(km);
        print(kcal);
      });
    } catch (Exception) {
      print('Error in Listen getSport:' + Exception);
    }
  }

  void getHealth() async {
    await getCharacteristic();
    await tx.write(dataSalud);
    await tx.write(dataSalud2);

    try {
      txNotify.value.listen((value) {
        // do something with new value
        if (value[1] == 3) {
          print(value[4]);
          print(value[5]);
          print(value[6]);

          hr = value[4];
          systolic = value[5];
          diastolic = value[6];
        }
        print(value);
      }).onError((e) {
        print(e);
      });
    } catch (Exception) {
      print('Error in Listen getHealth:' + Exception);
    }
  }

  void stopNotification() async {
    await tx.write(stopMeasuring, withoutResponse: true).whenComplete(() {
      tx.setNotifyValue(false);
    }).catchError((e) {
      print(e);
    });
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text(device.name),
        actions: <Widget>[
          StreamBuilder<BluetoothDeviceState>(
            stream: device.state,
            initialData: BluetoothDeviceState.connecting,
            builder: (c, snapshot) {
              VoidCallback onPressed;
              String text;
              switch (snapshot.data) {
                case BluetoothDeviceState.connected:
                  onPressed = () => device.disconnect();
                  text = 'DISCONNECT';
                  break;
                case BluetoothDeviceState.disconnected:
                  onPressed = () => device.connect();
                  text = 'CONNECT';
                  break;
                default:
                  onPressed = null;
                  text = snapshot.data.toString().substring(21).toUpperCase();
                  break;
              }
              return FlatButton(
                  onPressed: onPressed,
                  child: Text(
                    text,
                    style: Theme.of(context)
                        .primaryTextTheme
                        .button
                        .copyWith(color: Colors.white),
                  ));
            },
          )
        ],
      ),
      body: SingleChildScrollView(
        child: Column(
          children: <Widget>[
            StreamBuilder<BluetoothDeviceState>(
              stream: device.state,
              initialData: BluetoothDeviceState.connecting,
              builder: (c, snapshot) => ListTile(
                leading: (snapshot.data == BluetoothDeviceState.connected)
                    ? Icon(Icons.bluetooth_connected)
                    : Icon(Icons.bluetooth_disabled),
                title: Text(
                    'Device is ${snapshot.data.toString().split('.')[1]}.'),
                subtitle: Text('${device.id}'),
              ),
            ),
//            StreamBuilder<List<BluetoothService>>(
//              stream: device.services,
//              initialData: [],
//              builder: (c, snapshot) {
//                return Column(
//               children: _buildServiceTiles(snapshot.data),
//                );
//              },
//            ),

            RaisedButton(
              onPressed: () => Navigator.of(context).push(MaterialPageRoute(
                  builder: (context) => Data(device))),
            )
            /* StreamBuilder<List<int>>(
              stream: txNotify.value,
              initialData: [],
              builder: (c, snapShot) {
                List<int> a;

                if (snapShot.data != null || snapShot.hasData) {
                  a = snapShot.data;
                }
                return Text(a.toString());
              },
            )*/
          ],
        ),
      ),
    );
  }

  void setState(Null Function() param0) {}
}
